from core.api.serializers import UserShortSerializer
from django.contrib.auth.models import User
from rest_framework.viewsets import ModelViewSet


class UserViewSet(ModelViewSet):
    """Viewset для модели пользователя(User)."""

    serializer_class = UserShortSerializer
    queryset = User.objects.filter(is_active=True)
