from core.api.views import UserViewSet
from django.urls import include, path
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)
from server.real_estate_accounting.api import (
    urls as real_estate_accounting_urls,
)

app_name = "core"

router = DefaultRouter()
router.register("user-list", UserViewSet, basename="user-list")

urlpatterns = [
    path("", include(router.urls)),
    path(
        "real_estate_accounting/",
        include(
            real_estate_accounting_urls,
        ),
    ),
    path("token/", TokenObtainPairView.as_view(), name="token-obtain-pair"),
    path("token/refresh/", TokenRefreshView.as_view(), name="token-refresh"),
    path("token/verify/", TokenVerifyView.as_view(), name="token-verify"),
]
