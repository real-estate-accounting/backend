from django.contrib.auth.models import User
from rest_framework.serializers import (
    IntegerField,
    ModelSerializer,
    SerializerMethodField,
)


class UserShortSerializer(ModelSerializer):
    """Cериализатор модели пользователя (User)."""

    key = IntegerField(source="id")
    label = SerializerMethodField()

    class Meta:
        model = User
        fields = ["key", "label"]

    def get_label(self, obj):
        return f"{obj.first_name} {obj.last_name}"
