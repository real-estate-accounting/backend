import debug_toolbar
from core.api import urls as api_urls
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

app_name = "core"

urlpatterns = (
    [
        path("__debug__/", include(debug_toolbar.urls)),
        path("admin/", admin.site.urls),
        path("api-auth/", include("rest_framework.urls")),
        path(
            "api/",
            include(
                api_urls,
            ),
        ),
    ]
    + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
)
