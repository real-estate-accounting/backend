1) docker run --name portal -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=secret -e POSTGRES_DB=portal -d postgres:13.3

2) poetry shell

3) poetry install

4) export $(cat envs.django.example | xargs)

5) python manage.py migrate

6) python manage.py createsuperuser

Важно создать пользователя именно с этими параметрами, так как авторизация через JWT на фронте сделана фиктивно.

Вводим логин admin
Вводим почту test@test.ru
Вводим пароль secret


7) python manage.py runserver

8) Переходим на http://127.0.0.1:8000/admin - панель администратора
Переходим на http://127.0.0.1:8000/api/real_estate_accounting/- api