from datetime import datetime, timedelta

from django.contrib.auth.models import User
from django.db import models

MAX_LENHTH = 255


class District(models.Model):
    """Модель округа"""

    name = models.CharField(verbose_name="Название", max_length=MAX_LENHTH)

    class Meta:
        ordering = ["name"]
        verbose_name = "Округа"
        verbose_name_plural = "Округи"

    def __str__(self) -> str:
        """Строчное отображение записи."""
        return f"{self.name}"


class Region(models.Model):
    """Модель района"""

    name = models.CharField(verbose_name="Название", max_length=MAX_LENHTH)

    class Meta:
        ordering = ["name"]
        verbose_name = "Район"
        verbose_name_plural = "Районы"

    def __str__(self) -> str:
        """Строчное отображение записи."""
        return f"{self.name}"


class Type(models.Model):
    """Модель типа объекта"""

    name = models.CharField(verbose_name="Название", max_length=MAX_LENHTH)

    class Meta:
        ordering = ["name"]
        verbose_name = "Тип"
        verbose_name_plural = "Типы"

    def __str__(self) -> str:
        """Строчное отображение записи."""
        return f"{self.name}"


class Company(models.Model):
    """Модель исполнителя"""

    name = models.CharField(verbose_name="Название", max_length=MAX_LENHTH)

    class Meta:
        ordering = ["name"]
        verbose_name = "Компания"
        verbose_name_plural = "Компании"

    def __str__(self) -> str:
        """Строчное отображение записи."""
        return f"{self.name}"


class Status(models.Model):
    """Модель статуса"""

    name = models.CharField(verbose_name="Название", max_length=MAX_LENHTH)

    class Meta:
        ordering = ["name"]
        verbose_name = "Статус"
        verbose_name_plural = "Статусы"

    def __str__(self) -> str:
        """Строчное отображение записи."""
        return f"{self.name}"


class State(models.Model):
    """Модель состояния"""

    name = models.CharField(verbose_name="Название", max_length=MAX_LENHTH)

    class Meta:
        ordering = ["name"]
        verbose_name = "Состояние"
        verbose_name_plural = "Состояния"

    def __str__(self) -> str:
        """Строчное отображение записи."""
        return f"{self.name}"


class EstateObject(models.Model):
    """Модель объекта недвижимости"""

    address = models.CharField(verbose_name="Адрес", max_length=MAX_LENHTH)

    district = models.ForeignKey(
        District,
        on_delete=models.PROTECT,
        verbose_name="Округ",
    )

    region = models.ForeignKey(
        Region,
        on_delete=models.PROTECT,
        verbose_name="Район",
    )

    type = models.ForeignKey(
        Type,
        on_delete=models.PROTECT,
        verbose_name="Тип",
    )

    square = models.FloatField(verbose_name="Площадь")

    owner = models.CharField(verbose_name="Собственник", max_length=MAX_LENHTH)

    actual_user = models.CharField(
        verbose_name="Фактический пользователь", max_length=MAX_LENHTH
    )

    user = models.ForeignKey(
        User,
        verbose_name="Кто создал",
        on_delete=models.PROTECT,
    )

    status = models.ForeignKey(
        Status,
        on_delete=models.PROTECT,
        verbose_name="Статус",
    )

    cadastral_number = models.CharField(
        verbose_name="Кадастровый номер",
        max_length=MAX_LENHTH,
        blank=True,
        null=True,
    )

    is_industrial_zone = models.BooleanField(
        verbose_name="Находится в промзоне", default=False
    )

    protocol_number = models.CharField(
        verbose_name="Номер протокола",
        max_length=MAX_LENHTH,
        blank=True,
        null=True,
    )

    company = models.ForeignKey(
        Company,
        on_delete=models.PROTECT,
        verbose_name="Компания исполнитель",
        blank=True,
        null=True,
    )

    state = models.ForeignKey(
        State,
        on_delete=models.PROTECT,
        verbose_name="Состояние",
        blank=True,
        null=True,
    )

    db_date = models.DateTimeField(
        verbose_name="Дата создания", auto_now_add=True, auto_now=False
    )

    class Meta:
        ordering = ["pk", "district", "address", "db_date"]
        verbose_name = "Объект недвижимости"
        verbose_name_plural = "Объекты недвижимости"

    def __str__(self) -> str:
        """Строчное отображение записи."""
        return f"ID:{self.pk} {self.address}"


class AssigmentType(models.Model):
    """Модель типа объекта"""

    name = models.CharField(verbose_name="Название", max_length=MAX_LENHTH)

    class Meta:
        ordering = ["name"]
        verbose_name = "Тип поручения"
        verbose_name_plural = "Типы поручения"

    def __str__(self) -> str:
        """Строчное отображение записи."""
        return f"{self.name}"


class Period(models.Model):
    """Модель типа объекта"""

    name = models.CharField(verbose_name="Название", max_length=MAX_LENHTH)
    days = models.IntegerField(verbose_name="Кол-во дней")

    class Meta:
        ordering = ["name"]
        verbose_name = "Период"
        verbose_name_plural = "Периоды"

    def __str__(self) -> str:
        """Строчное отображение записи."""
        return f"{self.name}"


class AssigmentStatus(models.Model):
    """Модель статуса"""

    name = models.CharField(verbose_name="Название", max_length=MAX_LENHTH)

    class Meta:
        ordering = ["name"]
        verbose_name = "Статус поручения"
        verbose_name_plural = "Статусы поручения"

    def __str__(self) -> str:
        """Строчное отображение записи."""
        return f"{self.name}"


class Assigment(models.Model):
    """Модель поручения"""

    type = models.ForeignKey(
        AssigmentType,
        on_delete=models.PROTECT,
        verbose_name="Тип",
    )
    status = models.ForeignKey(
        AssigmentStatus,
        verbose_name="Статус",
        on_delete=models.PROTECT,
    )
    executor = models.ForeignKey(
        User,
        verbose_name="Исполнитель",
        on_delete=models.PROTECT,
    )

    estate_object = models.ForeignKey(
        EstateObject,
        on_delete=models.PROTECT,
        verbose_name="Объект недвижимости",
    )

    period = models.ForeignKey(
        Period,
        on_delete=models.PROTECT,
        verbose_name="Срок",
    )

    db_date = models.DateTimeField(
        verbose_name="Дата создания", auto_now_add=True, auto_now=False
    )

    class Meta:
        ordering = ["type"]
        verbose_name = "Поручение"
        verbose_name_plural = "Поручения"

    def __str__(self) -> str:
        """Строчное отображение записи."""
        return f"{self.executor}-{self.type}-{datetime.now().date() + timedelta(days=self.period.days)}"


class Meeting(models.Model):
    """Модель совещания"""

    title = models.TextField(verbose_name="Название")
    url = models.TextField(verbose_name="Название")
    start = models.DateTimeField(verbose_name="Дата совещания")
    json = models.JSONField(verbose_name="Json повестки")

    class Meta:
        ordering = ["title", "start"]
        verbose_name = "Совещание"
        verbose_name_plural = "Совещания"

    def __str__(self) -> str:
        """Строчное отображение записи."""
        return f"{self.title}-{self.start}"
