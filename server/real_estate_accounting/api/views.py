from datetime import date

from core.api.serializers import UserShortSerializer
from django.contrib.auth.models import User
from django.db import transaction
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from server.real_estate_accounting.api.serializers import (
    AssigmentSerializer,
    AssigmentSerializerCreateOrUpdateSerializer,
    AssigmentStatusSerializer,
    AssigmentTypeSerializer,
    CompanySerializer,
    DistrictSerializer,
    EstateObjectCreateOrUpdateSerializer,
    EstateObjectSerializer,
    MeetingCreateOrUpdateSerializer,
    MeetingSerializer,
    PeriodSerializer,
    RegionSerializer,
    StateSerializer,
    StatusSerializer,
    TypeSerializer,
)
from server.real_estate_accounting.models import (
    Assigment,
    AssigmentStatus,
    AssigmentType,
    Company,
    District,
    EstateObject,
    Meeting,
    Period,
    Region,
    State,
    Status,
    Type,
)


class TypeViewSet(ModelViewSet):
    """Viewset для модели типа объекта (Type)."""

    serializer_class = TypeSerializer
    queryset = Type.objects.all()


class RegionViewSet(ModelViewSet):
    """Viewset для модели район(Region)."""

    serializer_class = RegionSerializer
    queryset = Region.objects.all()


class StateViewSet(ModelViewSet):
    """Viewset для модели состояния(State)."""

    serializer_class = StateSerializer
    queryset = State.objects.all()


class StatusViewSet(ModelViewSet):
    """Viewset для модели статуса(Status)."""

    serializer_class = StatusSerializer
    queryset = Status.objects.all()


class CompanyViewSet(ModelViewSet):
    """Viewset для модели компании исполнителя(Company)."""

    serializer_class = CompanySerializer
    queryset = Company.objects.all()


class DistrictViewSet(ModelViewSet):
    """Viewset для модели округа(District)."""

    serializer_class = DistrictSerializer
    queryset = District.objects.all()


class EstateObjectViewSet(ModelViewSet):
    """Viewset для модели объекта недвижимости(EstateObject)."""

    serializer_class = EstateObjectSerializer
    queryset = EstateObject.objects.all().order_by("-id")

    @action(detail=False, methods=["post"])
    def createOrUpdateOrDelete(self, request):
        with transaction.atomic():
            if request.data.get("create"):
                data = request.data.get("create")
                data = [dict(item, user=request.user.id) for item in data]
                serializer_create = EstateObjectCreateOrUpdateSerializer(
                    data=data, many=True
                )
                if serializer_create.is_valid():
                    serializer_create.save()
                else:
                    return Response(
                        serializer_create.errors,
                        status=status.HTTP_400_BAD_REQUEST,
                    )
            if request.data.get("update"):
                data = request.data.get("update").get("items")
                data = [dict(item, user=request.user.id) for item in data]
                update = EstateObject.objects.filter(
                    id__in=request.data.get("update").get("ids")
                )
                if update.exists():
                    serializer_update = EstateObjectCreateOrUpdateSerializer(
                        update,
                        data=data,
                        many=True,
                    )
                    if serializer_update.is_valid():
                        serializer_update.save()
                    else:
                        return Response(
                            serializer_update.errors,
                            status=status.HTTP_400_BAD_REQUEST,
                        )
            if request.data.get("delete"):
                delete = EstateObject.objects.filter(
                    id__in=request.data.get("delete")
                )
                if delete.exists():
                    delete.delete()

            return Response({"action": "saved"})


class AssigmentTypeViewSet(ModelViewSet):
    """Viewset для модели типа поручения (AssigmentType)."""

    serializer_class = AssigmentTypeSerializer
    queryset = AssigmentType.objects.all()


class PeriodViewSet(ModelViewSet):
    """Viewset для модели периода (Period)."""

    serializer_class = PeriodSerializer
    queryset = Period.objects.all()


class AssigmentStatusViewSet(ModelViewSet):
    """Viewset для модели статуса поручения (AssigmentStatus)."""

    serializer_class = AssigmentStatusSerializer
    queryset = AssigmentStatus.objects.all()


class AssigmentViewSet(ModelViewSet):
    """Viewset для модели поручения (Assigment)."""

    serializer_class = AssigmentSerializer
    queryset = Assigment.objects.all()

    @action(detail=False, methods=["post"])
    def createOrUpdate(self, request):
        if request.data.get("id"):
            serializer = AssigmentSerializerCreateOrUpdateSerializer(
                Assigment.objects.get(id=request.data.get("id")),
                data=request.data,
            )
        else:
            serializer = AssigmentSerializerCreateOrUpdateSerializer(
                data=request.data
            )
        if serializer.is_valid():
            instance = serializer.save()
            serializer_instance = AssigmentSerializer(instance)
            return Response(
                {"action": "saved", "data": serializer_instance.data}
            )
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )


class MeetingViewSet(ModelViewSet):
    """Viewset для модели совещания (Meeting)."""

    serializer_class = MeetingSerializer
    queryset = Meeting.objects.all()

    @action(detail=False, methods=["post"])
    def createOrUpdate(self, request):
        if request.data.get("id"):
            serializer = MeetingCreateOrUpdateSerializer(
                Assigment.objects.get(id=request.data.get("id")),
                data=request.data,
            )
        else:
            serializer = MeetingCreateOrUpdateSerializer(data=request.data)
        if serializer.is_valid():
            instance = serializer.save()
            serializer_instance = MeetingSerializer(instance)
            return Response(
                {"action": "saved", "data": serializer_instance.data}
            )
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )


class AllDataAPIView(APIView):
    def get(self, request, format=None):
        type_list = Type.objects.all()
        region_list = Region.objects.all()
        state_list = State.objects.all()
        status_list = Status.objects.all()
        company_list = Company.objects.all()
        district_list = District.objects.all()
        estate_object_list = (
            EstateObject.objects.select_related(
                "status",
                "state",
                "user",
                "district",
                "region",
                "type",
                "company",
            )
            .prefetch_related("assigment_set")
            .order_by("-id")
        )
        assigment_type_list = AssigmentType.objects.all()
        period_list = Period.objects.all()
        user_list = User.objects.filter(is_active=True)
        assigment_status_list = AssigmentStatus.objects.all()
        meeting_list = Meeting.objects.filter(start__gte=date.today())
        type_serializer = TypeSerializer(type_list, many=True)
        region_serializer = RegionSerializer(region_list, many=True)
        state_serializer = StateSerializer(state_list, many=True)
        status_serializer = StatusSerializer(status_list, many=True)
        company_serializer = CompanySerializer(company_list, many=True)
        district_serializer = DistrictSerializer(district_list, many=True)
        estate_object_serializer = EstateObjectSerializer(
            estate_object_list, many=True
        )
        assigment_type_list_serializer = AssigmentTypeSerializer(
            assigment_type_list, many=True
        )
        period_list_serializer = PeriodSerializer(period_list, many=True)
        assigment_status_list_serializer = AssigmentStatusSerializer(
            assigment_status_list, many=True
        )
        user_list_serializer = UserShortSerializer(user_list, many=True)
        meeting_list_serializer = MeetingSerializer(meeting_list, many=True)
        data = {
            "type_list": type_serializer.data,
            "region_list": region_serializer.data,
            "state_list": state_serializer.data,
            "status_list": status_serializer.data,
            "company_list": company_serializer.data,
            "district_list": district_serializer.data,
            "estate_object_list": estate_object_serializer.data,
            "assigment_type_list": assigment_type_list_serializer.data,
            "period_list": period_list_serializer.data,
            "assigment_status_list": assigment_status_list_serializer.data,
            "user_list": user_list_serializer.data,
            "meeting_list": meeting_list_serializer.data,
        }

        return Response(data)
