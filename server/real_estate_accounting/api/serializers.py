from datetime import datetime, timedelta

from core.api.serializers import UserShortSerializer
from rest_framework.serializers import (
    CharField,
    IntegerField,
    ListSerializer,
    ModelSerializer,
    SerializerMethodField,
)
from server.real_estate_accounting.models import (
    Assigment,
    AssigmentStatus,
    AssigmentType,
    Company,
    District,
    EstateObject,
    Meeting,
    Period,
    Region,
    State,
    Status,
    Type,
)


class TypeSerializer(ModelSerializer):
    """Cериализатор модели типа объекта (Type)."""

    key = IntegerField(source="id")
    label = CharField(source="__str__")

    class Meta:
        model = Type
        fields = "__all__"


class RegionSerializer(ModelSerializer):
    """Cериализатор модели района (Region)."""

    key = IntegerField(source="id")
    label = CharField(source="__str__")

    class Meta:
        model = Region
        fields = "__all__"


class StatusSerializer(ModelSerializer):
    """Cериализатор модели статуса (Status)."""

    key = IntegerField(source="id")
    label = CharField(source="__str__")

    class Meta:
        model = Status
        fields = "__all__"


class StateSerializer(ModelSerializer):
    """Cериализатор модели состояния (State)."""

    key = IntegerField(source="id")
    label = CharField(source="__str__")

    class Meta:
        model = State
        fields = "__all__"


class DistrictSerializer(ModelSerializer):
    """Cериализатор модели округа (District)."""

    key = IntegerField(source="id")
    label = CharField(source="__str__")

    class Meta:
        model = District
        fields = "__all__"


class CompanySerializer(ModelSerializer):
    """Cериализатор модели компании исполнителя (Company)."""

    key = IntegerField(source="id")
    label = CharField(source="__str__")

    class Meta:
        model = Company
        fields = "__all__"


class PeriodSerializer(ModelSerializer):
    """Cериализатор периода (Period)."""

    key = IntegerField(source="id")
    label = CharField(source="__str__")

    class Meta:
        model = Period
        fields = "__all__"


class AssigmentTypeSerializer(ModelSerializer):
    """Cериализатор модели типа поручения (AssigmentType)."""

    key = IntegerField(source="id")
    label = CharField(source="__str__")

    class Meta:
        model = AssigmentType
        fields = "__all__"


class AssigmentStatusSerializer(ModelSerializer):
    """Cериализатор модели статуса поручения (AssigmentStatus)."""

    key = IntegerField(source="id")
    label = CharField(source="__str__")

    class Meta:
        model = AssigmentStatus
        fields = "__all__"


class AssigmentSerializer(ModelSerializer):
    """Cериализатор модели поручения (Assigment)."""

    key = IntegerField(source="id")
    label = CharField(source="__str__")
    type = AssigmentTypeSerializer()
    period = PeriodSerializer()
    executor = UserShortSerializer()
    status = AssigmentStatusSerializer()
    date = SerializerMethodField()

    class Meta:
        model = Assigment
        fields = "__all__"

    def get_date(self, obj):
        return datetime.now().date() + timedelta(days=obj.period.days)


class EstateObjectSerializer(ModelSerializer):
    """Cериализатор модели объекта недвижимости (EstateObject)."""

    key = IntegerField(source="id")
    label = CharField(source="__str__")
    status = StatusSerializer()
    state = StateSerializer()
    user = UserShortSerializer()
    district = DistrictSerializer()
    region = RegionSerializer()
    type = TypeSerializer()
    company = CompanySerializer()
    assigment_set = AssigmentSerializer(many=True)

    class Meta:
        model = EstateObject
        fields = "__all__"


class UpdateListSerializer(ListSerializer):
    """Cериализатор для множественного обновления экземпляров."""

    def update(self, instance, validated_data):
        item_mapping = {item.id: item for item in instance}
        data_mapping = {item["id"]: item for item in validated_data}

        ret = []
        for item_id, data in data_mapping.items():
            item = item_mapping.get(item_id, None)
            if item is None:
                ret.append(self.child.create(data))
            else:
                ret.append(self.child.update(item, data))

        for item_id, item in item_mapping.items():
            if item_id not in data_mapping:
                item.delete()

        return ret


class EstateObjectCreateOrUpdateSerializer(ModelSerializer):
    """Cериализатор для создания или обновления объекта недвижимости (EstateObject)."""

    id = IntegerField(required=False)

    class Meta:
        model = EstateObject
        list_serializer_class = UpdateListSerializer
        fields = "__all__"


class AssigmentSerializerCreateOrUpdateSerializer(ModelSerializer):
    """Cериализатор для создания или обновления поручения (Assigment)."""

    id = IntegerField(required=False)

    class Meta:
        model = Assigment
        list_serializer_class = UpdateListSerializer
        fields = "__all__"


class MeetingSerializer(ModelSerializer):
    """Cериализатор модели совещания (Meeting)."""

    key = IntegerField(source="id")
    label = CharField(source="__str__")

    class Meta:
        model = Meeting
        fields = "__all__"


class MeetingCreateOrUpdateSerializer(ModelSerializer):
    """Cериализатор для создания или обновления совещания (Meeting)."""

    id = IntegerField(required=False)

    class Meta:
        model = Meeting
        list_serializer_class = UpdateListSerializer
        fields = "__all__"
