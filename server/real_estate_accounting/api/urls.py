from django.urls import include, path
from rest_framework.routers import DefaultRouter
from server.real_estate_accounting.api.views import (
    AllDataAPIView,
    AssigmentStatusViewSet,
    AssigmentTypeViewSet,
    AssigmentViewSet,
    CompanyViewSet,
    DistrictViewSet,
    EstateObjectViewSet,
    MeetingViewSet,
    PeriodViewSet,
    RegionViewSet,
    StateViewSet,
    StatusViewSet,
    TypeViewSet,
)

app_name = "real_estate_accounting"

router = DefaultRouter()
router.register(
    "estate-object-list", EstateObjectViewSet, basename="estate-object-list"
)
router.register("status-list", StatusViewSet, basename="status-list")
router.register("state-list", StateViewSet, basename="state-list")
router.register("district-list", DistrictViewSet, basename="district-list")
router.register("company-list", CompanyViewSet, basename="company-list")
router.register("type-list", TypeViewSet, basename="type-list")
router.register("region-list", RegionViewSet, basename="region-list")
router.register("period-list", PeriodViewSet, basename="period-list")
router.register(
    "assigment-type-list", AssigmentTypeViewSet, basename="assigment-type-list"
)
router.register(
    "assigment-status-list",
    AssigmentStatusViewSet,
    basename="assigment-status-list",
)
router.register("assigment-list", AssigmentViewSet, basename="assigment-list")
router.register("meeting-list", MeetingViewSet, basename="meeting-list")

urlpatterns = [
    path("", include(router.urls)),
    path("all-data-list/", AllDataAPIView.as_view(), name="all-data-list"),
]
