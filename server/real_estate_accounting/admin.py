from django.contrib import admin
from server.real_estate_accounting.models import (
    Assigment,
    AssigmentStatus,
    AssigmentType,
    Company,
    District,
    EstateObject,
    Meeting,
    Period,
    Region,
    State,
    Status,
    Type,
)

admin.site.register(Type)
admin.site.register(Region)
admin.site.register(State)
admin.site.register(Status)
admin.site.register(EstateObject)
admin.site.register(Company)
admin.site.register(District)
admin.site.register(AssigmentType)
admin.site.register(Assigment)
admin.site.register(Period)
admin.site.register(AssigmentStatus)
admin.site.register(Meeting)
