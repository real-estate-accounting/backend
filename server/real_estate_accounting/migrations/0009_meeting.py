# Generated by Django 3.2.18 on 2023-04-09 19:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('real_estate_accounting', '0008_auto_20230409_1923'),
    ]

    operations = [
        migrations.CreateModel(
            name='Meeting',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(verbose_name='Название')),
                ('url', models.TextField(verbose_name='Название')),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='Дата совещания')),
                ('db_date', models.JSONField(verbose_name='Json повестки')),
            ],
            options={
                'verbose_name': 'Совещание',
                'verbose_name_plural': 'Совещания',
                'ordering': ['name', 'date'],
            },
        ),
    ]
