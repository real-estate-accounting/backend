from django.apps import AppConfig


class RealEstateAccountingConfig(AppConfig):
    """Конфигурация приложения 'Учет объектов недвижимости'."""

    name = "server.real_estate_accounting"
    verbose_name = "Учет объектов недвижимости"
